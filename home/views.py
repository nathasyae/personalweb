from django.shortcuts import render
import requests

# Create your views here.
def home(req):
    return render(req, 'home.html')
